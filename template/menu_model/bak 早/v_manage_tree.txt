﻿ 
  <div class="container col-lg-10 col-lg-offset-1">
<!--  
 全局开始   <div class="container col-lg-10 col-lg-offset-1">
结束
  -->
 
	<h1>CodeIgniter ACI Manage TREE</h1>
 



<!-- 导航栏开始-->

   <div class="col-lg-12">
            <nav class="navbar navbar-default navbar-inverse" role="navigation"   role="navigation">
                <div class="navbar-header">
                     
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                         <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>  

                    <a  class="navbar-brand" href={controller_addr}/index>   {tablename} </a> 
                </div>
                
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                   <?php
                   {menu_array}; 
                    //页面menu，来源于生成的添加 删除 编辑 
                    // 0=>read 1=>insert 2=>update 3=>delete 4=>help 5=>login
                     ?> 

                        <?php if ($menu_array[0]==true) { ?> 
                         <li>
                            <a href={controller_addr}/index>  <span class="glyphicon glyphicon-home"></span>首页</a> 
                        </li>
                        <?php }?>

                        <?php if ($menu_array[1]==true) { ?> 
                        <li>
                            <a href={controller_addr}/insert> <span class="glyphicon glyphicon-pencil "></span>插入数据</a>
                        </li>
                        <?php }?>

                        <?php if ($menu_array[3]==true) { ?> 
                        <li  class="active">
                            <a href={controller_addr}/manage>  <span class="glyphicon glyphicon-wrench "></span>管理</a>
                        </li>
                         <?php }?>

                        <?php if ($menu_array[4]==true) { ?> 
                        <li>
                            <a href={controller_addr}/help ><span class="glyphicon glyphicon-headphones"></span>帮助</a>
                        </li>
                        <?php }?>
                         
                    </ul>
                    
                        <?php if ($menu_array[5]==true) { ?> 
                    <ul class="nav navbar-nav navbar-right">

                        <li>
                            <a href="#">Help</a>
                        </li>
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">Action</a>
                                </li>
                                <li>
                                    <a href="#">Another action</a>
                                </li>
                                <li>
                                    <a href="#">Something else here</a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="#">Separated link</a>
                                </li>
                            </ul>
                        </li>
                    </ul>


                    <?php }?>


                </div>
                
            </nav>
        </div>
  
  <!-- 导航栏结束-->





         
 
<!--操作开始-->
     <div class="col-lg-12">   
	            
		    <label class="col-lg-1"> 全/不选<input type="checkbox" id="ckb_selectAll"  title="选中/取消选中"></label>

		    <label class="col-lg-2">反选<input type="checkbox" id="ckb_selectreverse" onclick="selectreverse()" title="反选"></label>
 
		    <label class="col-lg-2"> <a href="javascript:void(0);" onclick="del_()" title="删除选定数据" type="button" class="btn btn-danger btn-block">批量删除</a></label>

	 
  
 

                  <div class="col-lg-12"> 
                          <table  class="table table-hover dataTable"> 
                                        <?php echo $tree;?> 
                          </table>
            
                  </div>  

 
   
 

</div> 

 
<!--操作结束-->
 
 

<script>
  function success_opennewwindow(data) { 
         //登录成功后跳转 
           window.location.reload();
           // window.location = "<?php  if (isset($_SESSION["selfpage"])) { echo $_SESSION["selfpage"];     }?>";
        }




//全选 不选
$("#ckb_selectAll").click(function(){   
    if(this.checked){   
        $(".ckb:checkbox").prop("checked", true);  
    }else{   
	$(".ckb:checkbox").prop("checked", false);
    }   
});


   //反选 
    $("#ckb_selectreverse").click(function () { 
         $(".ckb:checkbox").each(function () {  
              $(this).prop("checked", !$(this).prop("checked"));  
         });
		// allchk();
    });



function del_() {
  var ids = '';
  $(".ckb").each(function() {
    if ($(this).is(':checked')) {
      ids += ',' + $(this).val(); //逐个获取id
    }
  });
  ids = ids.substring(1); // 对id进行处理，去除第一个逗号
  if (ids.length == 0) {
    alert('请选择要删除的选项');
  } else {
    if (confirm("确定删除？删除后将无法恢复。")) {
      url = "ids=" + ids;
      alert(url);
      $.ajax({
        type: "GET",
       	url: "{controller_addr}/delall/",
      //  url: "/codep2.php",
        dataType:'json',
        data: url, 
              success:function(result) {
                    if(result.counts == 1) {


                       alert(result.counts+result.des+result.ids);
                         setTimeout("success_opennewwindow()",500);
                        //重新加载本页面
                    	//alert(result.counts);
                    	//alert(result.des);
                       // obj.parents('form').submit(); //验证码正确提交表单
                    }else{
                       alert(result.counts+result.des+result.ids);
                    	 
                    }
                },
                error: function(XMLHttpRequest, textStatus) {
        	  alert("页面请求错误，请检查重试或联系管理员！\n" + textStatus);
       		 }


      });
    }
  }
}
 

</script>


 



<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm1').bootstrapValidator({
        message: 'This value is not valid',
        container: '#errors',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
           
            postitem: {
                message: 'The item is not valid',
                validators: {
                    notEmpty: {
                        message: 'The item is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 10,
                        message: 'The item must be more than 2 and less than 30 characters long'
                    }
                }
            }
        }
    });
});
</script>
















<!--  
 全局结束 <div class="container col-lg-9 col-lg-offset-2">
结束
  -->
  </div>




