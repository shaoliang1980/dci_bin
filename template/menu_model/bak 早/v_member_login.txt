﻿

    <script type="text/javascript" src="/lib/jquery.form.js "></script>


    <script type="text/javascript">
        $(document).ready(function() { 
  
    // bind form using ajaxForm 
    $('#defaultForm').ajaxForm({ 

        beforeSubmit: validate, 
        // dataType identifies the expected content type of the server response 
         dataType:  'json', 
         error:back_error,
        // success identifies the function to invoke when the server response 
        success: back_success 
         })
    });

        function back_success(data) { 
 
          //$("#htmlExampleTarget").html('Ok:'+data.message); 
            if (data.status=="loginsuccess") {
                  $('.alert').html('登录成功！'+data.message).attr("class","alert alert-success").show();   
                  setTimeout("success_opennewwindow()",1000);

            }else if (data.status=="loginfail") {
                  $('.alert').html('登录失败'+data.message).attr("class","alert alert-danger").show();  
                   $('.alert').delay(3000).hide(0);   
            }
        
        }

         function success_opennewwindow(data) { 
         //登录成功后跳转 
            window.location = "<?php   echo  site_url()."/member/index" ?>";
        }


         function back_error(data) {  
            $('.alert').html('jquery.form.js错误，请检查member代码').attr("class","alert alert-danger").show();
        }

        function validate(formData, jqForm, options) {  
            var form = jqForm[0]; 
            if (!form.username.value || !form.password.value) { 
             
              $('.alert').html('用户名111、密码不能为空').attr("class","alert alert-danger").show();
              $('.alert').delay(3000).hide(0);

                return false; 
            } 

          //  alert('Both fields contain values.'); 
        }

    </script> 



    <div id="htmlExampleTarget"></div>



 <div class="container">
    <div class="row">
        <!-- form: -->
        <section>
            <div class="col-lg-8 col-lg-offset-2">
                <div class="page-header">
                    <h2>登录系统</h2>
                </div>
              
         <form id="defaultForm" method="post" class="form-horizontal" action="<?php   echo  site_url()."/member/check_post_login" ?>" >    
                    <div class="alert alert-success" style="display: none;"></div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Username</label>
                        <div class="col-lg-5">
                            <input type="text" class="form-control" name="username" />
                        </div>
                    </div>

               

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Password</label>
                        <div class="col-lg-5">
                            <input type="password" class="form-control" name="password" />
                        </div>
                    </div> 
                 <div class="form-group">
                            <label class="col-lg-3 control-label">验证码2</label>
                            <div class="col-lg-3">
                               <input   name="code" id="mod-captcha-code"  class="form-control  pull-left"  type="text">
                                <span class="help-block" id="confirmcode_tips"  />
                            </div>
                            <div class="col-lg-4">
                              <img class="code-img" style="height:30px;width:80px;" src="http://localhost/drcms/lib/code.php?t=0" onclick="this.src=this.src.substring(0,this.src.indexOf('?')+1)+Math.random();return false;" />
                            </div>
                        </div>


                    <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-3">
                            <button type="submit" class="btn btn-primary">  登  录  </button>
                            <button id="submitBtn" type="submit"   class="btn btn-primary" >提交验证码</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- :form -->
    </div>
</div>


 

<script>
(function($){
    $(document).ready(function(){
        $("#submitBtn").click(function() {
            var obj = $(this);
            $.ajax({
                url:'{libpath}/codep2.php',
                type:'POST',
                data:{code:$.trim($("input[name=code]").val())},
                dataType:'json',
                async:false,
                success:function(result) {
                    if(result.status == 1) {
                        obj.parents('form').submit(); //验证码正确提交表单
                    }else{
                        $(".code-img").click();
                        $("#confirmcode_tips").html('验证码错误！');
                        setTimeout(function(){
                            $("#confirmcode_tips").empty();
                        },3000);
                    }
                },
                error:function(msg){
                    $("#confirmcode_tips").html('Error:'+msg.toSource());
                }
            })
            return false;
        })
    });
})(jQuery);
</script> 