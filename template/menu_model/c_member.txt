﻿<?php
//class Member extends My_Controller{
//class Member extends Admin_Controller{
//class Member extends Normal_Controller{
class Member extends CI_Controller{
       public function __construct()
       {
            parent::__construct();
            // Your own constructor code
            
            $this->load->helper('form');
         $this->load->library('form_validation');
         $this->load->library('session');
         $this->load->database("default");       
         $this->load->helper('url');   
         $this->load->model("m_member");
             //加载 url lib
       }

 function login()
    {

        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->database("default");       
        $this->form_validation->set_error_delimiters('', '');   
        
        $config = array(
          array("field"   =>"username","label"   => "username","rules"   => "trim|required|callback_username_check"),
          array("field"   =>"password","label"   => "password","rules"   => "trim|required|callback_password_check"),
   
        );
            $this->form_validation->set_rules($config);;
        
        //设置错误定界符
        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');

        $this->_username = $this->input->post('username');
        $this->_password = $this->input->post('password');      

   
        //echo $_SESSION["username"];
                //用户名
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('member/edit_header');
            $this->load->view('member/login');
        }
        else 
        {
            //注册session,设定登录状态
            $this->m_member->create_loginsession($this->_username, $this->_password); 

             $data['message'] = $this->session->userdata('username').' You are logged in! Now take a look at the '
                              .anchor('account/dashboard', 'Dashboard');

            $this->load->view('member/note', $data); 
        }
  }



//通过cookie判断是否登录
function check_sessionlogin() {
 
        if ($this->m_member->check_session_islogined()==TRUE) {
            return true;
        }else{
              return false;
                $data['message'] = $this->session->userdata('username').'没有登录，请登录';
            $this->load->view('member/login', $data); 
        } 
 
}

function check_post_login() { 
 //通过登录提交过来的页面
  if (@isset($_SESSION["loginfailtimes"])) {
        $_SESSION["loginfailtimes"]++;
        if ( $_SESSION["loginfailtimes"]>5) {
           echo json_encode(array("message"=>"你已经登录超过5次，不能登录","status"=>"loginfail")); 
            exit();
        }
  }else{
    //60秒内不允许在登录
     $this->session->set_tempdata('loginfailtimes',"1",60);
  }


  if (@isset($_POST["username"])&&@isset($_POST["password"])) {
        $username=$_POST["username"];
        $password=$_POST["password"]; 
 
    
        if ($this->m_member->check_islogined($username,md5($password))==TRUE) {
        
             echo json_encode(array("message"=>"欢迎你". $username.",跳转中","status"=>"loginsuccess")); 
            exit();
        }else{ 
             echo json_encode(array("message"=>"登录失败，你已经登录".$_SESSION["loginfailtimes"]."次","status"=>"loginfail")); 
          
            exit();
           // echo json_encode(array('url'=>"/","msg"=>"登录失败","filetype"=>"2"));   
        } 
    }else {
     // echo json_encode(array('url'=>"/","msg"=>"登录失败","filetype"=>"3"));   
    }
}




function check_login() {

//echo json_encode(array("message"=>$_POST['username'],"type"=>"1")); 
 echo json_encode(array("message"=>$_POST['username'],"type"=>"1")); 
exit();
 
// echo json_encode(array('url'=>"/","msg"=>"登录成功","filetype"=>"1"));

 
}



//登录表单验证时自定义的函数
/**
     * 提示用户名是不存在的登录
     * @param string $username
     * @return bool 
     */
    function username_check($username)
    {
        if ($this->m_member->get_by_username($username))
        {
            return TRUE;
        }
        else 
        {
            $this->form_validation->set_message('username_check', '用户名不存在');
            return FALSE;
        }
    }
    /**
     * 检查用户的密码正确性
     */
    function password_check($password)
    {
        $password = md5($this->input->post('password'));      
        if ($this->m_member->password_check($this->_username, $password))
        {
            return TRUE;
        }
        else 
        {
            $this->form_validation->set_message('password_check', '用户名或密码不正确');
            return FALSE;
        }
    }


function register(){
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->database("default");       
        $this->form_validation->set_error_delimiters('', '');   
        
        $config = array(
    array("field"   =>"username","label"   => "username","rules"   => "trim|required|callback_username_exists"),
    array("field"   =>"password_conf","label"   => "password_conf","rules"   => "trim|required"),
    array("field"   =>"password","label"   => "password","rules"   => "trim|required|min_length[4]|max_length[12]
|matches[password_conf]"),
    array("field"   =>"email","label"   => "email","rules"   => "trim|required")
    // array("field"   =>"email","label"   => "email","rules"   => "trim|required|valid_email")
);
    $this->form_validation->set_rules($config);;
        
         if ($this->form_validation->run() == FALSE)
        {           
               //  $this->load->view("account/insert");
       $this->load->view("member/edit_header");
       $this->load->view("member/register");
       $this->load->view("member/edit_footer");
        }
        else
        { 
        $this->load->helper('url');
        $this->load->model('m_member','',TRUE);
        $this->m_member->insert(); 

// 定位到 添加成功后的页面
          $this->load->view("member/note");


    //  redirect('account/insertOk','refresh'); 
        }

}


function username_exists($username)
    {
        if ($this->m_member->get_by_username($username))
        {
            $this->form_validation->set_message('username_exists', '用户名已被占用');
            return FALSE;
        }
        return TRUE;
    }



/**
     * 用户退出
     * 已经登录则退出，否者转到details
     */
    function loginout()
    {
        if ($this->m_member->logout() == TRUE)
        {
            $this->load->view('member/logout');
        }
        else
        {
            $this->load->view('member/index');
        }
    
    }






    
function index(){
    $this->load->database("default");   
    $this->load->library('pagination');  
    $config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/index/';     
    $config['total_rows'] = $this->db->count_all('member');     
    $config['per_page'] = '10';  
    $this->pagination->initialize($config);     
    $this->load->model("m_member");
    $data['query']=$this->m_member->read($config['per_page'],$this->uri->segment(3));   


    //传递总页数等，通过session传递
    $this->session->set_userdata('total_rows', $config['total_rows']);



    $this->load->view("member/read_header",$data);
    $this->load->view("member/read",$data);
    $this->load->view("member/read_footer",$data);
}
function read(){
    $this->load->database("default");   
    $this->load->library('pagination');  
    $config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/read/';      
    $config['total_rows'] = $this->db->count_all('member');     
    $config['per_page'] = '10';  
    $this->pagination->initialize($config);     
    $this->load->model("m_member");
    $data['query']=$this->m_member->read($config['per_page'],$this->uri->segment(3));

    //传递总页数等，通过session传递
    $this->session->set_userdata('total_rows', $config['total_rows']);


    $this->load->view("member/read_header",$data);
    $this->load->view("member/read",$data);
    $this->load->view("member/read_footer",$data);

}

 



function search(){

 if(!isset($_POST['postitem'])||empty($_POST['postitem'])){ 
        echo " searchitem为空";
        $postitem="";       
        exit;
    }else{
     $postitem=$_POST["postitem"];
        //echo $postitem;
    }


    $this->load->database("default");   
    $this->load->library('pagination');  
    $config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/search/';    


     $this->db->like('username', $postitem);
     //field_search 为默认搜索字段

     $this->db->from('member');   
     $config['total_rows']=$this->db->count_all_results();  
    $config['per_page'] = '10';  
    $this->pagination->initialize($config);     
    $this->load->model("m_member");
     
    //传递总页数等，通过session传递
    $this->session->set_userdata('total_rows', $config['total_rows']);


    $data['query']=$this->m_member->search($postitem,$config['per_page'],$this->uri->segment(3));   


//  $config['total_rows'] = $this->db->count_all('member');     
    //$config['per_page'] = '10';    
//  $this->pagination->initialize($config);     
    //$this->load->model("m_member");
    //$data['query']=$this->m_member->read($config['per_page'],$this->uri->segment(3)); 
    
    $this->load->view("member/read_header",$data);
    $this->load->view("member/read",$data);
    $this->load->view("member/read_footer",$data);

}







//管理搜索
function managesearch(){

 if(!isset($_POST['postitem'])||empty($_POST['postitem'])){ 
        echo " searchitem为空";
        $postitem="";       
        exit;
    }else{
     $postitem=$_POST["postitem"];
        //echo $postitem;
    }


    $this->load->database("default");   
    $this->load->library('pagination');  
    $config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/search/';    


     $this->db->like('username', $postitem);
     //field_search 为默认搜索字段

     $this->db->from('member');   
     $config['total_rows']=$this->db->count_all_results();  
    $config['per_page'] = '10';  
    $this->pagination->initialize($config);     
    $this->load->model("m_member");
     
    //传递总页数等，通过session传递
    $this->session->set_userdata('total_rows', $config['total_rows']);


    $data['query']=$this->m_member->search($postitem,$config['per_page'],$this->uri->segment(3));   

 
    
    $this->load->view("member/manage_header",$data);
        $this->load->view("member/manage",$data); 
    $this->load->view("member/manage_footer",$data);
}








//管理模块，多项目删除
function manage(){
    $this->load->database("default");   
    $this->load->library('pagination');  
    $config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/manage/';    
    $config['total_rows'] = $this->db->count_all('member');     
    $config['per_page'] = '10';  
    $this->pagination->initialize($config);     
    $this->load->model("m_member");
    $data['query']=$this->m_member->read($config['per_page'],$this->uri->segment(3));   
    
    
    $this->load->view("member/manage_header",$data);
        $this->load->view("member/manage",$data); 
    $this->load->view("member/manage_footer",$data);


}



function insert(){
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->database("default");       
        $this->form_validation->set_error_delimiters('', '');   
        
        $config = array(
    array("field"   =>"username","label"   => "username","rules"   => "required"),
    array("field"   =>"password","label"   => "password","rules"   => "required"),
    array("field"   =>"email","label"   => "email","rules"   => "required")
);
$this->form_validation->set_rules($config);;
        
         if ($this->form_validation->run() == FALSE)
        {           
               //  $this->load->view("member/insert");
       $this->load->view("member/edit_header");
       $this->load->view("member/insert");
      $this->load->view("member/edit_footer");
        }
        else
        { 
        $this->load->helper('url');
        $this->load->model('m_member','',TRUE);
        $this->m_member->insert(); 

// 定位到 添加成功后的页面

       $this->load->view("member/edit_header");
      $this->load->view("member/submitok");
      $this->load->view("member/edit_footer");


    //  redirect('member/insertOk','refresh');  
        }

}


 



function summernoteupload(){

      //summernoteupload 图片上传
 

            list($usec, $sec) = explode(" ", microtime());
               $filename = ((float)$usec + (float)$sec) * 1000;
             $filename=str_replace(".", "", $filename);     
                $tmp = explode('.', $_FILES['file']['name']);
                $tmp =strtolower(end($tmp));

                //判断文件类型
                $strtype1="gif|jpg|png";
                $strtype2="doc|zip|pdf";

                if (strpos($strtype1, $tmp) !== false) {
                  $filetype="1";
                }else if (strpos($strtype2, $tmp) !== false) {
                  $filetype="2";
                }


        //生成要生成的文件名
                $filename = $filename.'.'.$tmp; 
                //  上传之前的文件名
                $pre_filename= $_FILES['file']['name'];

                $folder = date("Ymd");
                $folder ="uploads/". $folder;

                //如果上传的文件夹不存在，则创建之
                if ($folder) {
                    @mkdir($folder);
                }

                //文件目录
                $folder = $folder . '/img/';

                //如果上传的文件夹不存在，则创建之
                if ($folder) {
                    @mkdir($folder);
                }

               //图片上传的具体路径就出来了
             //   $destination = $targetDir_url . $filename; //change this directory
              $filepath=base_url().$folder.$filename;//上一页


          $config['upload_path']      =$folder;
          $config['allowed_types']    = 'gif|jpg|png|doc|zip|pdf';
          $config['max_size']     = 10000;
          $config['max_width']        = 10241;
          $config['max_height']       = 7681;
           $config['file_name']= $filename;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        { 
          //上传错误
        //  echo json_encode(array('error' => $this->upload->display_errors(),'url'=>'/1.png'));
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
      
             echo json_encode(array('url'=>"$filepath","a"=>"$filepath","filetype"=>"$filetype","pre_filename"=>"$pre_filename"));
    
           // $this->load->view('type/upload_success', $data);
       
        }
 
    exit;


}





function insertOk(){
    
    //成功后返回操作
        $segs = $this->uri->segment_array();
 
        echo "<div  style='width:600;height:300;padding:100; margin:100;background:silver'>";
        echo "<h1><li>新建操作成功 </li></h1><br>";        
        echo "<h3><li>".base_url()."index.php/".$segs[1]."/".$segs[2]."<a href=".base_url()."index.php/".$segs[1]."/".$segs[2].">返回上一页</a></li>";       

        echo "<li>".base_url()."index.php/".$segs[1]."<a href=".base_url()."index.php/".$segs[1].">返回首页</a></li> ";

        echo "<li>".base_url()."index.php<a href=".base_url()."index.php>返回网站 首页</a></li></h3>";  

        //echo '<meta http-equiv="refresh" content="3;URL=".base_url()."index.php/".$segs[1]."/".$segs[2]."><li>三秒后返回前页</li>';

        echo "<meta http-equiv='refresh' content='3;URL=".base_url()."index.php/".$segs[1]."/'><li>三秒后返回前页</li>";

        echo "</div>";
        


    }

function update($a=-1){
        if ($a<0) { 
    exit('Update 后面参数不能为空');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');        
        $this->load->database("default");
        $this->form_validation->set_error_delimiters('', '');   
 
 
        $config = array(
    array("field"   =>"username","label"   => "username","rules"   => "required"),
    array("field"   =>"password","label"   => "password","rules"   => "required"),
    array("field"   =>"email","label"   => "email","rules"   => "required")
);
$this->form_validation->set_rules($config);;

         if ($this->form_validation->run() == FALSE)
        {           
                    //  $this->load->view("member/update");
       $this->load->database("default");
       $this->load->model("m_member");
       $data['query']=$this->m_member->get1($a);
       $this->load->view("member/edit_header");
       $this->load->view("member/update",$data);
      $this->load->view("member/edit_footer");
        }
        else
        { 
        $this->load->helper('url');
        $this->load->model('m_member','',TRUE);

            $this->m_member->update($a); 
        

  // 定位到 添加成功后的页面
  
       $this->load->view("member/edit_header");
      $this->load->view("member/submitok");
      $this->load->view("member/edit_footer");
 // redirect('member/updateOk','refresh');  



        }

}
 


function updateOk(){
  
   
      

  }




function delete($a=-1){
    if ($a<0) { 
        exit('Delete 后面参数不能为空');
                }
                
    $this->load->database("default");
    $this->db->where("member_id ",$a);
    $this->db->delete("member");
    $this->load->helper('url'); 

// 定位到 添加成功后的页面
      $this->load->view("member/edit_header");
      $this->load->view("member/submitok");
        //redirect('member/deleteOk','refresh');    


    }



function delall(){  

    //删除多个id的数据
    //得到从ajax传过来的 数据

    if(isset($_REQUEST['ids'])){
        //将字符改为数组 where in 只能接受数组
        $ids=explode(',',$_REQUEST['ids']);
    }else{ 
        $counts = "0";
        $des = "post没有"; 
        exit();

    } 

    $this->load->database("default"); 
    $this->db->where_in("member_id ", $ids);     
    //$this->load->helper('url');   

    if ($this->db->delete("member")) {
        $counts = "1";
        $des = "成功";
    } 
    else {
        $counts = "0";
        $des = "失败";
    }  
 
 $json_data="{";
 $json_data.="\"counts\":".json_encode($counts).",";
 $json_data.="\"des\":".json_encode($des).",";  
 $json_data.="\"ids\":".json_encode($ids)."";
 $json_data.="}";
 echo $json_data; 
//break; 
  }





    function deleteOk(){

    //成功后返回操作
        $segs = $this->uri->segment_array();
 
        echo "<div  style='width:600;height:300;padding:100; margin:100;background:silver'>";
        echo "<h1><li>删除操作成功  </li></h1><br>";           
        echo "<h3><li>".base_url()."index.php/".$segs[1]."/".$segs[2]."<a href=".base_url()."index.php/".$segs[1]."/".$segs[2].">返回上一页</a></li>";       

        echo "<li>".base_url()."index.php/".$segs[1]."<a href=".base_url()."index.php/".$segs[1].">返回首页</a></li> ";

        echo "<li>".base_url()."index.php<a href=".base_url()."index.php>返回网站 首页</a></li></h3>";  

        //echo '<meta http-equiv="refresh" content="3;URL=".base_url()."index.php/".$segs[1]."/".$segs[2]."><li>三秒后返回前页</li>';

        echo "<meta http-equiv='refresh' content='3;URL=".base_url()."index.php/".$segs[1]."/'><li>三秒后返回前页</li>";

        echo "</div>";
        

    } 
function itemlist($a){
    $this->load->database("default");
    $this->load->model("m_member");
    $data['query']=$this->m_member->get1($a);
    
    $this->load->view("member/itemlist_header",$data);
    $this->load->view("member/itemlist",$data);
    $this->load->view("member/itemlist_footer",$data);

}



}
