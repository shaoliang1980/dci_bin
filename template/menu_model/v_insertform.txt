﻿ 

</head>

<body>

  <div class="container col-lg-10 col-lg-offset-1">
<!--  
 全局开始   <div class="container col-lg-10 col-lg-offset-1">
结束
  -->


    <h1>CodeIgniter ACI Insert</h1>
 









<!-- 导航栏开始-->

   <div class="col-lg-12">
            <nav class="navbar navbar-default navbar-inverse" role="navigation"   role="navigation">
                <div class="navbar-header">
                     
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                         <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>  

                    <a  class="navbar-brand" href={controller_addr}/index>     {tablename}</a> 
                </div>
                
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                   <?php
                   {menu_array}; 
                    //页面menu，来源于生成的添加 删除 编辑 
                    // 0=>read 1=>insert 2=>update 3=>delete 4=>help 5=>login
                     ?> 

                        <?php if ($menu_array[0]==true) { ?> 
                         <li>
                            <a href={controller_addr}/index>  <span class="glyphicon glyphicon-home"></span>首页</a> 
                        </li>
                        <?php }?>

                        <?php if ($menu_array[1]==true) { ?> 
                        <li class="active">
                            <a href={controller_addr}/insert> <span class="glyphicon glyphicon-pencil "></span>插入数据</a>
                        </li>
                        <?php }?>

                        <?php if ($menu_array[3]==true) { ?> 
                        <li >
                            <a href={controller_addr}/manage>  <span class="glyphicon glyphicon-wrench "></span>管理</a>
                        </li>
                         <?php }?>

                        <?php if ($menu_array[4]==true) { ?> 
                        <li>
                            <a href={controller_addr}/help ><span class="glyphicon glyphicon-headphones"></span>帮助</a>
                        </li>
                        <?php }?>
                         
                    </ul>
                    
                        <?php if ($menu_array[5]==true) { ?> 
                    <ul class="nav navbar-nav navbar-right">

                        <li>
                            <a href="#">Help</a>
                        </li>
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                                           





                                                                                         <li>
                                <a href="<?php  echo site_url().'/article/';?> "  >article</a>
                              </li>

                              <li>
                                    <a href="<?php  echo site_url().'/type/';?> "  >type</a>
                              </li>





                                <li>
                                    <a href="#">Another action</a>
                                </li>
                                <li>
                                    <a href="#">Something else here</a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="#">Separated link</a>
                                </li>
                            </ul>
                        </li>
                    </ul>


                    <?php }?>


                </div>
                
            </nav>
        </div>
  
  <!-- 导航栏结束-->












 
   <!-- 表单开始-->
      <!-- 表单div开始-->
            <div class="col-lg-12 col-lg-offset-0">


                <div class="page-header">
                    <h2>数据{tablename}</h2>
                    <p class="lead">数据添加操作</p>
                </div>



 
	       {forminput}
   


                 <!--  模板moudle_tree-->
               <div class="form-group">
                        <label class="col-lg-1 control-label">Url  </label>
                        <div class="col-lg-5"> 

                               
                            <select name="url" class="form-control">
                        
                              <?php echo $moudle_tree;?>  
                            </select>
                        </div>
                    </div>  

                  <!--  moudle_tree结束-->
   
   
             <!--目录循环-->
                <div class="form-group">
                        <label class="col-lg-1 control-label">pid  </label>
                        <div class="col-lg-5"> 

                               
                            <select name="pid" class="form-control">
                            <option value="0">根目录</option>
                              <?php echo $tree;?>  
                            </select>
                        </div>
                    </div>  

              <!--目录循环结束-->


                    <div class="form-group">
                         <label class="col-lg-1 control-label" id="captchaOperation"></label>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" name="captcha" />
                        </div>
                    </div>

 
            <!--  验证码2 图片验证码
                     <div class="form-group">
                            <label class="col-lg-3 control-label">验证码2</label>
                            <div class="col-lg-3">
                               <input   name="code" id="mod-captcha-code"  class="form-control  pull-left"  type="text">
                                <span class="help-block" id="confirmcode_tips"  />
                            </div>
                            <div class="col-lg-4">
                              <img class="code-img" style="height:30px;width:80px;" src="http://localhost/drcms/lib/code.php?t=0" onclick="this.src=this.src.substring(0,this.src.indexOf('?')+1)+Math.random();return false;" />
                            </div>
                        </div>
          验证码2结束      -->

  
     

                    <div class="form-group">
                        <div class="col-lg-9 col-lg-offset-1">
        <!-- 验证码2  如果使用这个验证码，需要将form id 修改，否则2次验证不能提交

                    <button id="submitBtn" type="submit"   class="btn btn-primary" >提交</button>
        -->

             <!--防止重复提交-->
            <input type="hidden" name="submittime" id="submittime" value="<?php  $_SESSION['submittime']=time(); echo $_SESSION['submittime'];?>">
              <!--防止重复提交结束-->

                          <button type="submit" class="btn btn-primary">Submit</button> 
                        </div>
                    </div>

 

                </form>






      <!-- 表单div结束-->
   </div>







<script type="text/javascript">
$(document).ready(function() {
    // Generate a simple captcha
    function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    function generateCaptcha() {
        $('#captchaOperation').html([randomNumber(1, 10), '+', randomNumber(1, 10), '='].join(' '));
    };
    generateCaptcha();

    $('#form')
        .bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                {formjs}
                
          



                captcha: {
                    validators: {
                        callback: {
                            message: 'Wrong answer',
                            callback: function(value, validator) {
                                var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                                return value == sum;
                            }
                        }
                    }
                }
            }
        })
        .on('error.form.bv', function(e) {
            var $form              = $(e.target),
                bootstrapValidator = $form.data('bootstrapValidator');

            if (!bootstrapValidator.isValidField('captcha')) {
                // The captcha is not valid
                // Regenerate the captcha
                generateCaptcha();
            }
        });
});
</script>












    <!--    验证码2，图片验证码

<script>
(function($){
    $(document).ready(function(){
        $("#submitBtn").click(function() {
            var obj = $(this);
            $.ajax({
                url:'{libpath}/codep2.php',
                type:'POST',
                data:{code:$.trim($("input[name=code]").val())},
                dataType:'json',
                async:false,
                success:function(result) {
                    if(result.status == 1) {
                        obj.parents('form').submit(); //验证码正确提交表单
                    }else{
                        $(".code-img").click();
                        $("#confirmcode_tips").html('验证码错误！');
                        setTimeout(function(){
                            $("#confirmcode_tips").empty();
                        },3000);
                    }
                },
                error:function(msg){
                    $("#confirmcode_tips").html('Error:'+msg.toSource());
                }
            })
            return false;
        })
    });
})(jQuery);
</script> 

-->



    <!--    添加 百度uedit  放置于合适位置
    <div class="col-lg-12">
    <textarea rows="0" cols="0"  style="resize:none;" id="ed1" type="" name="type"> </textarea>   
    </div>
-->



    <!--    添加 百度uedit
    
  <script type="text/javascript"> 
  var ue = UE.getEditor('ed1',{
  autoHeight: false,initialFrameHeight:400,initialFrameWidth:800, 
  autoHeightEnabled: true,
  autoFloatEnabled: true
});
</script>
 
-->











  <!-- 表单栏结束-->
































<!--  
 全局结束 <div class="container col-lg-9 col-lg-offset-2">
结束
  -->
  </div>