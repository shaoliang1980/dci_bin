﻿<?php
class M_member extends CI_Model{
   function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }




  function create_loginsession($username,$password)
    {
        $data = array('username'=>$username, 'password'=>md5($password), 'logged_in'=>TRUE);
        $this->session->set_userdata($data);                    //添加session数据
        }

    /**
     * 通过用户名获得用户记录
     * @param string $username
     */
    function get_by_username($username)
    {
        $this->db->where('username', $username);
        $query = $this->db->get('member');
        //return $query->row();                            //不判断获得什么直接返回
        if ($query->num_rows() == 1)
        {
             return $query->row();
        }
        else
        {
            return FALSE;
        }
    }
    
 
    function check_session_islogined()
    {

       if (@isset($_SESSION["username"])&&@isset($_SESSION["password"])) {
        $username=$_SESSION["username"];
        $password=$_SESSION["password"]; 

        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get('member');
        //return $query->row();                            //不判断获得什么直接返回
          if ($query->num_rows() == 1)
          {
             //  return $query->row();
            return true;
          }
          else
          {
              return FALSE;
          }
      }else{ return FALSE;}
    }
    
 
    function check_islogined($username,$password)
    {

       
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get('member');
        //return $query->row();                            //不判断获得什么直接返回
          if ($query->num_rows() == 1)
          {
             //  return $query->row();
            return true;
          }
          else
          {
              return FALSE;
          }
        
    }
    


 
    /**
     * 用户名不存在时,返回false
     * 用户名存在时，验证密码是否正确
     */
    function password_check($username, $password)
    {                
        if($user = $this->get_by_username($username))
        {
            return $user->password == $password ? TRUE : FALSE;
        }
        return FALSE;                                    //当用户名不存在时
    }



  function logout()
    {

      if (isset($_SESSION["logged_in"])) {
             if ($_SESSION["logged_in"]=== TRUE)
            {
                $this->session->sess_destroy();                //销毁所有session的数据
                return TRUE;
            }
            return FALSE;
      }
    
    }





  //得到一个符合主键的信息
  function get1($a){
   
   $this->db->where("member_id ",$a);

  $query=$this->db->get("member");

  return $query->result();
  }


    function read($num,$offset){

  //$this->db->select('*');
  //$this->db->from('blogs');
  //$this->db->join('comments', 'comments.id = blogs.id');
  //$query = $this->db->get();
  //$this->db->order_by('id DESC, name ASC');
  //$this->db->where(a,b);


//member_id ,username,password,email,) 为所有的字段列表
  $this->db->select('member_id ,username,password,email,');

  $this->db->order_by('member_id  DESC');
  $query=$this->db->get("member",$num,$offset);
  return $query->result();


  }


  
  
    function search($postitem="",$num=10,$offset=0){

    // $this->db->select('username,member_id,password');
  //$this->db->order_by('id DESC, name ASC');
      // $this->db->select('username,member_id,password');

//member_id ,username,password,email,) 为所有的字段列表

    $this->db->select('member_id ,username,password,email,');


//username) 为默认搜索字段，默认第二个为搜索字段

   $this->db->like('username', $postitem);
  $this->db->order_by('member_id  Desc');
 
  $query=$this->db->get('member',$num,$offset);
   return $query->result();
  }




  function insert(){
  $username = $this->input->post("username",true);
  $password = md5($this->input->post("password",true));
  $email = $this->input->post("email",true);
  $data=array("username" => $username,"password" => $password,"email" => $email );
  $this->db->insert("member",$data);



  }




  function update($a){
  $username = $this->input->post("username",true);
  $password = $this->input->post("password",true);
  $email = $this->input->post("email",true);
  $data=array("username" => $username,"password" => $password,"email" => $email );
  $this->db->where("member_id ",$a);
  $this->db->update("member",$data);
  }




  function delete($a){
  $this->db->delete($a);


  }





}