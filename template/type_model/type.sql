﻿# Host: localhost  (Version: 5.5.53)
# Date: 2017-09-14 16:23:48
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "type"
#

DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) NOT NULL DEFAULT '0',
  `is_show` tinyint(2) DEFAULT '0',
  `pid` int(11) DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;

#
# Data for table "type"
#

/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (161,'文章管理',0,0),(166,'心得',0,161);
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
