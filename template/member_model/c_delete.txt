﻿
function delete($a=-1){
	if ($a<0) { 
		exit('Delete 后面参数不能为空');
				}
				
	{dbconnect};
	$this->db->where("{id}",$a);
	$this->db->delete("{tbname}");
	$this->load->helper('url'); 

// 定位到 添加成功后的页面
      $this->load->view("{vpath}/edit_header");
      $this->load->view("{vpath}/submitok");
  		//redirect('{mcon}/deleteOk','refresh');	


	}



function delall(){  

	//删除多个id的数据
	//得到从ajax传过来的 数据

	if(isset($_REQUEST['ids'])){
	 	//将字符改为数组 where in 只能接受数组
		$ids=explode(',',$_REQUEST['ids']);
	}else{ 
 		$counts = "0";
 	 	$des = "post没有"; 
  		exit();

	} 

	$this->load->database("default"); 
	$this->db->where_in("{id}", $ids);	 
	//$this->load->helper('url');  	

	if ($this->db->delete("{tbname}")) {
 		$counts = "1";
 		$des = "成功";
	} 
	else {
 		$counts = "0";
  		$des = "失败";
	}  
 
 $json_data="{";
 $json_data.="\"counts\":".json_encode($counts).",";
 $json_data.="\"des\":".json_encode($des).",";  
 $json_data.="\"ids\":".json_encode($ids)."";
 $json_data.="}";
 echo $json_data; 
//break; 
  }





	function deleteOk(){

	//成功后返回操作
		$segs = $this->uri->segment_array();
 
		echo "<div  style='width:600;height:300;padding:100; margin:100;background:silver'>";
		echo "<h1><li>删除操作成功  </li></h1><br>";  		 
  		echo "<h3><li>".base_url()."index.php/".$segs[1]."/".$segs[2]."<a href=".base_url()."index.php/".$segs[1]."/".$segs[2].">返回上一页</a></li>";  		

  		echo "<li>".base_url()."index.php/".$segs[1]."<a href=".base_url()."index.php/".$segs[1].">返回首页</a></li> ";

  		echo "<li>".base_url()."index.php<a href=".base_url()."index.php>返回网站 首页</a></li></h3>";  

  		//echo '<meta http-equiv="refresh" content="3;URL=".base_url()."index.php/".$segs[1]."/".$segs[2]."><li>三秒后返回前页</li>';

  		echo "<meta http-equiv='refresh' content='3;URL=".base_url()."index.php/".$segs[1]."/'><li>三秒后返回前页</li>";

  		echo "</div>";
  		

	}









//管理搜索
function managesearch(){

 if(!isset($_POST['postitem'])||empty($_POST['postitem'])){ 
 		echo " searchitem为空";
		$postitem="";		
		exit;
	}else{
	 $postitem=$_POST["postitem"];
	 	//echo $postitem;
	}


	{dbconnect};	
	$this->load->library('pagination');  
	$config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/search/';  	


	 $this->db->like('{field_search}', $postitem);
	 //field_search 为默认搜索字段

	 $this->db->from('{tb}');   
	 $config['total_rows']=$this->db->count_all_results();  
	$config['per_page'] = '{perpage}';	 
	$this->pagination->initialize($config); 	
	$this->load->model("{model_name}");
	 
	//传递总页数等，通过session传递
	//$this->session->set_userdata('total_rows', $config['total_rows']);
	$data['total_rows']=$config['total_rows'];

	$data['query']=$this->{model_name}->search($postitem,$config['per_page'],$this->uri->segment(3));	

 
	
	$this->load->view("{directory}/manage_header",$data);
		$this->load->view("{directory}/manage",$data); 
	$this->load->view("{directory}/manage_footer",$data);
}








//管理模块，多项目删除
function manage(){
	{dbconnect};	
	$this->load->library('pagination');  
	$config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/manage/';  	
	$config['total_rows'] = $this->db->count_all('{tb}');     
	$config['per_page'] = '{perpage}';	 
	$this->pagination->initialize($config); 	
	$this->load->model("{model_name}");
	$data['query']=$this->{model_name}->read($config['per_page'],$this->uri->segment(3));	
	
	$data['total_rows']=$config['total_rows'];


	$this->load->view("{directory}/manage_header",$data);
		$this->load->view("{directory}/manage",$data); 
	$this->load->view("{directory}/manage_footer",$data);


}






function manage_tree(){	
		//输出树形结构 
	$this->load->library('tree'); 	 
	{dbconnect};	
 	//加载model
	$this->load->model("{model_name}");


	$selfpage=site_url().$this->uri->slash_segment(1,'leading');//control	 
 	 $data1 =$this->{model_name}->get_all();		 	 

    //转换数据  
    $tree_data=array();  
    foreach ($data1 as $key=>$value){  
    //树形列表需要3个参数  id pid name
        $tree_data[$value->id]=array(  
            'id'=> $value->id,  
            'parentid'=>$value->pid,  
            'name'=>$value->type  
        );  
    }  
      
    /** 
     * 输出树形结构 
     */  
    $str="<tr>  
        <td><input type='checkbox' class='ckb' id='list[\$id]' value='\$id'></td>  
        <td>\$id</td>  
        <td>\$spacer\$name</td>  
        <td><a href='".	$selfpage."/insert/\$id'>添加</a></td>  
        <td><a href='".	$selfpage."/delete/\$id'>删除</a></td>  
        <td><a href='".	$selfpage."/update/\$id'>修改</a></td>  
        </tr>";  
      
     $tree=new Tree();  
     $tree->nbsp = "&nbsp&nbsp";  


    $tree->init($tree_data);  
 
    $data["tree"]=$tree->get_tree(0, $str);
  //  $data["tree"]=$tree->get_tree(0, $str,2); 

 	 $this->load->view("{directory}/manage_header",$data);
	 $this->load->view("{directory}/manage_tree",$data);
	 $this->load->view("{directory}/manage_footer",$data);

      
    /** 
     * 输出下拉列表 
     */   
   // $str="<option value=\$id \$selected>\$spacer\$name</option>";  
  //  $tree=new Tree();  
  //  $tree->init($tree_data); 

 //   echo "<select>";  
  //  echo $tree->get_tree(0, $str,2);  
   // echo "</select>";   
 
}








