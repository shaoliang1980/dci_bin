﻿ 

  <div class="container col-lg-9 col-lg-offset-2">
<!--  
 全局开始 <div class="container col-lg-9 col-lg-offset-2">
结束
  -->
 
	<h1>CodeIgniter ACI Manage</h1>
 





<!-- 导航栏开始-->

   <div class="col-lg-12">
            <nav class="navbar navbar-default navbar-inverse" role="navigation"   role="navigation">
                <div class="navbar-header">
                     
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                         <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>  

                    <a  class="navbar-brand" href={mcon}/index> {tablename} </a> 
                </div>
                
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                   <?php
                   {menu_array}; 
                    //页面menu，来源于生成的添加 删除 编辑 
                    // 0=>read 1=>insert 2=>update 3=>delete 4=>help 5=>login
                     ?> 

                        <?php if ($menu_array[0]==true) { ?> 
                         <li>
                            <a href={mcon}/index>  <span class="glyphicon glyphicon-home"></span>首页</a> 
                        </li>
                        <?php }?>

                        <?php if ($menu_array[1]==true) { ?> 
                        <li>
                            <a href={mcon}/insert> <span class="glyphicon glyphicon-pencil "></span>插入数据</a>
                        </li>
                        <?php }?>

                        <?php if ($menu_array[3]==true) { ?> 
                        <li  class="active">
                            <a href={mcon}/manage>  <span class="glyphicon glyphicon-wrench "></span>管理</a>
                        </li>
                         <?php }?>

                        <?php if ($menu_array[4]==true) { ?> 
                        <li>
                            <a href={mcon}/help ><span class="glyphicon glyphicon-headphones"></span>帮助</a>
                        </li>
                        <?php }?>
                         
                    </ul>
                    
                        <?php if ($menu_array[5]==true) { ?> 
                    <ul class="nav navbar-nav navbar-right">

                        <li>
                            <a href="#">Help</a>
                        </li>
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">Action</a>
                                </li>
                                <li>
                                    <a href="#">Another action</a>
                                </li>
                                <li>
                                    <a href="#">Something else here</a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="#">Separated link</a>
                                </li>
                            </ul>
                        </li>
                    </ul>


                    <?php }?>


                </div>
                
            </nav>
        </div>
  
  <!-- 导航栏结束-->








 
    <!-- 搜索栏开始-->

      <div class="col-lg-12">
                <div class="col-lg-4">
                    <form  id="defaultForm1" method="post" action="{mcon}/managesearch"  role="form" class="form-inline">
                        <div class="form-group">
                             
                            <label for="exampleInputEmail1">
                               Postitem
                            </label>
                            <input type="text" class="form-control" name="postitem" />
                        </div>
                        
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button> 
                        
                    </form>
                </div> 
                    <div class="form-group col-lg-4">                     
                          <span id="errors"> </span>                 
                    </div>

                     <div class="form-group col-lg-4">
                         <ul class="nav nav-pills">
                                <li  >
                                    <a href="#"> <span class="badge pull-right"> 
                                     <?php

                                      if (isset($total_rows)) {
                                      //总页数                                            
                                             echo $total_rows;
                                        }
 
 
                            ?></span> Total</a>
                                </li>
                                 
                            </ul>

                          </div>
        </div>
 
 <!-- 搜索栏结束-->
         

 <?php echo $this->pagination->create_links(); ?> 
 
 <!-- 循环开始-->
              <div class="col-md-12">   
	            
		<label class="col-md-1"> 全/不选<input type="checkbox" id="ckb_selectAll"  title="选中/取消选中"></label>

		<label class="col-md-2">反选<input type="checkbox" id="ckb_selectreverse" onclick="selectreverse()" title="反选"></label>
 
		<label class="col-md-2"> <a href="javascript:void(0);" onclick="del_()" title="删除选定数据" type="button" class="btn btn-danger btn-block">批量删除</a></label>

	<?php 
	foreach ($query as $row){   ?> 
		  {rowlist}   
	
	<?php

	}
	?> 
              


    </div>


 
 <!-- 循环结束-->
  

 <!--
manage_tree 树形结构列表

                  <div class="col-lg-12"> 
                          <table  class="table table-hover dataTable"> 
                                        <?php echo $tree;?> 
                          </table>
            
                  </div>  

 -->
   
 


 

 

<script>
  function success_opennewwindow(data) { 
         //登录成功后跳转 
           window.location.reload();
           // window.location = "<?php  if (isset($_SESSION["selfpage"])) { echo $_SESSION["selfpage"];     }?>";
        }




//全选 不选
$("#ckb_selectAll").click(function(){   
    if(this.checked){   
        $(".ckb:checkbox").prop("checked", true);  
    }else{   
	$(".ckb:checkbox").prop("checked", false);
    }   
});


   //反选 
    $("#ckb_selectreverse").click(function () { 
         $(".ckb:checkbox").each(function () {  
              $(this).prop("checked", !$(this).prop("checked"));  
         });
		// allchk();
    });



function del_() {
  var ids = '';
  $(".ckb").each(function() {
    if ($(this).is(':checked')) {
      ids += ',' + $(this).val(); //逐个获取id
    }
  });
  ids = ids.substring(1); // 对id进行处理，去除第一个逗号
  if (ids.length == 0) {
    alert('请选择要删除的选项');
  } else {
    if (confirm("确定删除？删除后将无法恢复。")) {
      url = "ids=" + ids;
      alert(url);
      $.ajax({
        type: "GET",
       	url: "<?PHP echo base_url().index_page().$this->uri->slash_segment(1,'both').'delall';?>",
      //  url: "/codep2.php",
        dataType:'json',
        data: url, 
              success:function(result) {
                    if(result.counts == 1) {


                       alert(result.counts+result.des+result.ids);
                         setTimeout("success_opennewwindow()",500);
                        //重新加载本页面
                    	//alert(result.counts);
                    	//alert(result.des);
                       // obj.parents('form').submit(); //验证码正确提交表单
                    }else{
                       alert(result.counts+result.des+result.ids);
                    	 
                    }
                },
                error: function(XMLHttpRequest, textStatus) {
        	  alert("页面请求错误，请检查重试或联系管理员！\n" + textStatus);
       		 }


      });
    }
  }
}
 

</script>


 



<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm1').bootstrapValidator({
        message: 'This value is not valid',
        container: '#errors',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
           
            postitem: {
                message: 'The item is not valid',
                validators: {
                    notEmpty: {
                        message: 'The item is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 10,
                        message: 'The item must be more than 2 and less than 30 characters long'
                    }
                }
            }
        }
    });
});
</script>
















<!--  
 全局结束 <div class="container col-lg-9 col-lg-offset-2">
结束
  -->
  </div>




