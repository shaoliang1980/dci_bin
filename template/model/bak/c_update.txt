﻿function update($a=-1){
		if ($a<0) { 
    exit('Update 后面参数不能为空');
        }

		$this->load->helper('form');
		$this->load->library('form_validation');		
		{dbconnect};
		$this->form_validation->set_error_delimiters('', ''); 	 
 
		{validation_config};

	     if ($this->form_validation->run() == FALSE)
  		{   		
                    //  {false_action_content};
       $this->load->database("default");
       $this->load->model("{modelname}");
       $data['row']=$this->{modelname}->get1($a);
       $this->load->view("{vpath}/edit_header");
       $this->load->view("{vpath}/update",$data);
      $this->load->view("{vpath}/edit_footer");
  		}
  		else
  		{ 
  		$this->load->helper('url');
  		$this->load->model('{modelname}','',TRUE);

	        $this->{modelname}->update($a); 		

      // 定位到 添加成功后的页面
  
      $this->load->view("{vpath}/edit_header");
      $this->load->view("{vpath}/submitok");
      $this->load->view("{vpath}/edit_footer");
       // redirect('{mcon}/updateOk','refresh');  
  		}

}
 






function update_tree($a=-1){
  //生成树形结构的update ，适合于menu type等
  // 数据结构  id type pid

    if ($a<0) { 
    exit('Update 后面参数不能为空');
        }

  $this->load->helper('form');
  $this->load->database("default"); 

  //加载model

  $this->load->model("{modelname}");



  //输出树形结构 

  $this->load->library('tree');  

  $selfpage=site_url().$this->uri->slash_segment(1,'leading');  
  //control    
  $data1 =$this->{modelname}->get_all(); 

    //转换数据  
    $tree_data=array();  
    foreach ($data1 as $key=>$value){  
    //树形列表需要3个参数  id pid name 排除本身id
     if ($value->id!=$a) {
            $tree_data[$value->id]=array(  
                'id'=> $value->id,  
                'parentid'=>$value->pid,  
                'name'=>$value->type  
            );  
        }
    }  

	$str="<option value=\$id \$selected>\$spacer\$name</option>";         
	$tree=new Tree();  
	$tree->nbsp = "&nbsp&nbsp"; 
	$tree->init($tree_data);   
	$data["tree"]=$tree->get_tree(0, $str,$this->{modelname}->get_parentid($a));

  //输出树形结构 结束
  

    $this->load->library('form_validation');  
    $this->form_validation->set_error_delimiters('', '');   
 
 
        {validation_config};


       if ($this->form_validation->run() == FALSE)
      {       

      //打开update页面

      // $data['query']=$this->{modelname}->get1($a); //循环读取
       $data['row']=$this->{modelname}->get1($a); 
       $this->load->view("{vpath}/edit_header");
       $this->load->view("{vpath}/update",$data);
       $this->load->view("{vpath}/edit_footer");
      }
      else
      { 

      //成功后修改数据
      $this->load->helper('url');
      $this->load->model('{modelname}','',TRUE);
      $this->{modelname}->update($a); 

       // 定位到 添加成功后的页面  
       $this->load->view("{vpath}/edit_header");
       $this->load->view("{vpath}/submitok");
       $this->load->view("{vpath}/edit_footer"); 
 
      }

}
 





function updateOk(){
  
   
      

  }



