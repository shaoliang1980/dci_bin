﻿//得到一个符合主键的信息
	function get1($a){
   
	 $this->db->where("{id}",$a);

	$query=$this->db->get("{tb}");
	return $query->row(); //不用循环读取
	//return $query->result();    //循环读取
	}







	//得到一个符合pid的信息
	function get_parentid($a=-1){
   
	 $this->db->where("{id}",$a);
	$query1=$this->db->get("{tb}");
	// 获取pid
	return $query1->row()->pid;  
	 
	}


	//得到一个符合id 父pid的信息 适合于 用品pid 的多重递归目录，比如列表，menu等等
	function get1_parent_row($a=-1){
   
	 $this->db->where("{id}",$a);
	$query1=$this->db->get("{tb}");
	//先获取pid
	$pid=$query1->row()->pid; 


	//获取id 为pid的信息
	$this->db->where("{id}",$pid);	 
	$query2=$this->db->get("{tb}");
	$parent_row=$query2->row();

	return $parent_row;
	// echo "pid:$pid. $parent_row->type" ; 
	}





	function get_count($a=0){
   //获取符合条件的数量
	 $this->db->where("{id}",$a);

	 //$names = array('Frank', 'Todd', 'James');
	//$this->db->or_where_in('username', $names);
	//$this->db->where_not_in('username', $names);
	 ////$this->db->where_in('username', $names);


	$query=$this->db->from("{tb}");
	return $this->db->count_all();
	}


	function get_all(){
   //获取所有数据 
   //	 $this->db->where("{id}",$a);
   //	 $this->db->where("{id}",$a);
   //$this->db->select('title, content, date');
   //$this->db->select_min('age');
   //$this->db->select_sum('age');
   //$this->db->select_avg('age');
	//$this->db->order_by('{id} DESC');
	// $this->db->like('{field_search}', $postitem);


	//join
	//$this->db->select('*');
	//$this->db->from('blogs');
	//$this->db->join('comments', 'comments.id = blogs.id');
	////$this->db->join('comments', 'comments.id = blogs.id', 'left');
	//$query = $this->db->get();


	//end join


	//$this->db->group_by(array("title", "date")); 


	//$this->db->cache_off();
	//	$this->db->cache_cache_delete_all();

	$query=$this->db->get("{tb}");
 
	 return $query->result();


	}




	function get_fromsql($str='select * from {tb}'){
    $query = $this->db->query($str);
 	//echo $query->num_rows();
	return $query->result();
	}


