﻿
  <div class="container col-lg-10 col-lg-offset-1">
<!--  
 全局开始 
  <div class="container col-lg-10 col-lg-offset-1">
结束
  -->
 
	<h1>CodeIgniter ACI Read</h1>
 



<!-- 导航栏开始-->

   <div class="col-lg-12">
            <nav class="navbar navbar-default navbar-inverse" role="navigation"   role="navigation">
                <div class="navbar-header">
                     
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                         <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>  

                    <a  class="navbar-brand" href={controller_addr}/index> {tablename} </a> 
                </div>
                
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                   <?php
                   {menu_array}; 
                    //页面menu，来源于生成的添加 删除 编辑 
                    // 0=>read 1=>insert 2=>update 3=>delete 4=>help 5=>login
                     ?> 

                        <?php if ($menu_array[0]==true) { ?> 
                         <li class="active">
                            <a href={controller_addr}/index>  <span class="glyphicon glyphicon-home"></span>首页</a> 
                        </li>
                        <?php }?>

                        <?php if ($menu_array[1]==true) { ?> 
                        <li>
                            <a href={controller_addr}/insert> <span class="glyphicon glyphicon-pencil "></span>插入数据</a>
                        </li>
                        <?php }?>

                        <?php if ($menu_array[3]==true) { ?> 
                        <li >
                            <a href={controller_addr}/manage>  <span class="glyphicon glyphicon-wrench "></span>管理</a>
                        </li>
                         <?php }?>

                        <?php if ($menu_array[4]==true) { ?> 
                        <li>
                            <a href={controller_addr}/help ><span class="glyphicon glyphicon-headphones"></span>帮助</a>
                        </li>
                        <?php }?>
                         
                    </ul>
                    
                        <?php if ($menu_array[5]==true) { ?> 
                    <ul class="nav navbar-nav navbar-right">

                        <li>
                            <a href="#">Help</a>
                        </li>
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">Action</a>
                                </li>
                                <li>
                                    <a href="#">Another action</a>
                                </li>
                                <li>
                                    <a href="#">Something else here</a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="#">Separated link</a>
                                </li>
                            </ul>
                        </li>
                    </ul>


                    <?php }?>


                </div>
                
            </nav>
        </div>
  
  <!-- 导航栏结束-->


 
    <!-- 搜索栏开始-->

      <div class="col-lg-12">
                <div class="col-lg-6">
                    <form  id="defaultForm1" method="post" action="{controller_addr}/search"  role="form" class="form-inline">
                        <div class="form-group">
                             
                            <label for="exampleInputEmail1">
                               Postitem
                            </label>
                            <input type="text" class="form-control" name="postitem" />
                        </div>
                        
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button> 
                        
                    </form>
                </div> 
                    <div class="form-group col-lg-3">                     
                          <span id="errors"> </span>                 
                    </div>

                     <div class="form-group col-lg-2">
                         <ul class="nav nav-pills">
                                <li  >
                                     <a href="#"> <span class="badge pull-right"> 
                                     <?php

                                      if (isset($total_rows)) {
                                      //总页数                                            
                                             echo $total_rows;
                                        }

                         //   if (isset($_SESSION["total_rows"])) {
                        //         echo $_SESSION["total_rows"];
                        //       //  unset($_SESSION['total_rows']);
                        //    }
 
                            ?></span> Total</a>
                                </li>
                                 
                            </ul>

                          </div>
        </div>
 
 <!-- 搜索栏结束-->
         

 
 <!-- 循环开始-->
        <div class="col-md-12">            
                <?php     foreach ($query as $row){   ?>

                        {rowlist} 

                <?php   } ?>         
                
    </div>


 
 <!-- 循环结束-->
  

 
   
 
 <?php echo $this->pagination->create_links(); ?> 


 




<script type="text/javascript">
$(document).ready(function() {
    $('#defaultForm1').bootstrapValidator({
        message: 'This value is not valid',
        container: '#errors',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
           
            postitem: {
                message: 'The item is not valid',
                validators: {
                    notEmpty: {
                        message: 'The item is required and cannot be empty'
                    },
                    stringLength: {
                        min: 2,
                        max: 10,
                        message: 'The item must be more than 2 and less than 30 characters long'
                    }
                }
            }
        }
    });
});
</script>
















<!--  
 全局结束 <div class="container col-lg-9 col-lg-offset-2">
结束
  -->
  </div>