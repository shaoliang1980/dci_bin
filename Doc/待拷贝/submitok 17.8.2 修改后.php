﻿<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
 
      <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
     <script src="/bootstrap/js/jquery.js"></script>
      <!-- 包括所有已编译的插件 -->
      <script src="/bootstrap/js/bootstrap.min.js"></script> 
    <style type="text/css">
     
        .modal {
            top: 10%;
            bottom: auto;           
            padding: 0;
            background-color: #ffffff;
            border: 1px solid #999999;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
            box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
            background-clip: padding-box;
            overflow-y: auto;
        }
        .modal.container {
            max-width: none;
        }
        #firstmodal {
            width: 70%;
            height: 70%;
        }
        #secondmodal {
            width: 70%;
            height: 70%;
        }
    </style>
    <script type="text/javascript">
        $(function() {
        	 $('#secondmodal').modal();
            $('#m1').on("click", function() {
                $('#firstmodal').modal();
            });
            $('#m2').on("click", function () {
                $('#secondmodal').modal();
            });
        });
    </script>
</head>
<body>
        <div class="content" style="margin-left: 100px;margin-top: 100px;">
            <button class="btn btn-primary btn-lg" id="m1">打开第一层模态窗口</button>

            <?php
 
	//成功后返回操作 
	//这个地方那个用 base_url()会自动换为 127.0.0.1 需要设置 ci的config文件为 localhost
 		//$mainpage="http://".$_SERVER["SERVER_NAME"]."/".index_page();//网站首页 

		$mainpage=base_url().index_page();
 		$prepage=$mainpage.$this->uri->slash_segment(1,'leading').$this->uri->slash_segment(2,'both');//上一页
 		$indexpage=$mainpage.$this->uri->slash_segment(1,'both');//首页
 		
		echo "<div >";
		echo "<h1><li>操作成功 </li></h1><br>";  		 

  		echo "<h3><li>".$prepage."<a href=".$prepage.">返回上一页</a></li>";   
  		echo "<li>".$indexpage."<a href=".$indexpage.">返回首页</a></li> ";
  		echo "<li>".$mainpage."<a href=".$mainpage.">返回网站 首页</a></li></h3>";   
  		//echo "<meta http-equiv='refresh' content='3;URL=".$indexpage."'><li>三秒后返回前页...</li>";

  		echo "</div>";
  		
	?>
	</ul>
	</div>
        </div>
 
        <div id="firstmodal" class="modal container fade" tabindex="-1" style="display: auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">第一层模态窗口</h4>
            </div>
            <div class="modal-body">
                <p>
                    <button class="btn btn-primary btn-lg" id="m2">打开第二层模态窗口</button>
                    第一层主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容
                    主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容
                    主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容
                    主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容主体内容
                 
                </p>
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
            </div>
        </div>
        <div id="secondmodal" class="modal container fade" tabindex="-1" style="display: none;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">第二层模态窗口</h4>
            </div>
            <div class="modal-body">
                <p>
                    <div class="MainContent">
	<ul>
	<?php
 
	//成功后返回操作 
	//这个地方那个用 base_url()会自动换为 127.0.0.1 需要设置 ci的config文件为 localhost
 		//$mainpage="http://".$_SERVER["SERVER_NAME"]."/".index_page();//网站首页 

		$mainpage=base_url().index_page();
 		$prepage=$mainpage.$this->uri->slash_segment(1,'leading').$this->uri->slash_segment(2,'both');//上一页
 		$indexpage=$mainpage.$this->uri->slash_segment(1,'both');//首页
 		
		echo "<div >";
		echo "<h1><li>操作成功 </li></h1><br>";  		 

  		echo "<h3><li>".$prepage."<a href=".$prepage.">返回上一页</a></li>";   
  		echo "<li>".$indexpage."<a href=".$indexpage.">返回首页</a></li> ";
  		echo "<li>".$mainpage."<a href=".$mainpage.">返回网站 首页</a></li></h3>";   
  		//echo "<meta http-equiv='refresh' content='3;URL=".$indexpage."'><li>三秒后返回前页...</li>";

  		echo "</div>";
  		
	?>
	</ul>
	</div>
                </p>
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" data-dismiss="modal" class="btn btn-default">关闭</button>
               
		<?php  echo  "<a class=\"btn btn-default\" href=".$prepage.">返回上一页</a> ";   ?>
            </div>
        </div>
 

 