﻿<?php
class Account extends CI_Controller{
	   public function __construct()
       {
            parent::__construct();
            // Your own constructor code
			  $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->database("default");       
             $this->load->helper('url');   
    $this->load->model("m_account");
			 //加载 url lib
       }



 function login()
    {
 
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->database("default");       
        $this->form_validation->set_error_delimiters('', '');   
        
        $config = array(
    array("field"   =>"username","label"   => "username","rules"   => "trim|required|callback_username_check"),
    array("field"   =>"password","label"   => "password","rules"   => "trim|required|callback_password_check"),
   
);
$this->form_validation->set_rules($config);;
        
        //设置错误定界符
        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');

        $this->_username = $this->input->post('username');      

   
        //echo $_SESSION["username"];
                //用户名
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('account/login');
        }
        else 
        {
            //注册session,设定登录状态
            $this->m_account->login($this->_username);
            $data['message'] = $this->session->userdata('username').' You are logged in! Now take a look at the '
                              .anchor('account/dashboard', 'Dashboard');
            $this->load->view('account/note', $data);


        }
  }

//登录表单验证时自定义的函数
/**
     * 提示用户名是不存在的登录
     * @param string $username
     * @return bool 
     */
    function username_check($username)
    {
        if ($this->m_account->get_by_username($username))
        {
            return TRUE;
        }
        else 
        {
            $this->form_validation->set_message('username_check', '用户名不存在');
            return FALSE;
        }
    }
    /**
     * 检查用户的密码正确性
     */
    function password_check($password)
    {
        $password = md5($this->input->post('password'));      
        if ($this->m_account->password_check($this->_username, $password))
        {
            return TRUE;
        }
        else 
        {
            $this->form_validation->set_message('password_check', '用户名或密码不正确');
            return FALSE;
        }
    }


function register(){
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->database("default");       
        $this->form_validation->set_error_delimiters('', '');   
        
        $config = array(
    array("field"   =>"username","label"   => "username","rules"   => "trim|required|callback_username_exists"),
    array("field"   =>"password_conf","label"   => "password_conf","rules"   => "trim|required"),
    array("field"   =>"password","label"   => "password","rules"   => "trim|required|min_length[4]|max_length[12]
|matches[password_conf]"),
    array("field"   =>"email","label"   => "email","rules"   => "trim|required")
    // array("field"   =>"email","label"   => "email","rules"   => "trim|required|valid_email")
);
$this->form_validation->set_rules($config);;
        
         if ($this->form_validation->run() == FALSE)
        {           
               //  $this->load->view("account/insert");
       $this->load->view("account/edit_header");
       $this->load->view("account/register");
      $this->load->view("account/edit_footer");
        }
        else
        { 
        $this->load->helper('url');
        $this->load->model('m_account','',TRUE);
        $this->m_account->insert(); 

// 定位到 添加成功后的页面
      $this->load->view("account/note");


    //  redirect('account/insertOk','refresh'); 
        }

}


function username_exists($username)
    {
        if ($this->m_account->get_by_username($username))
        {
            $this->form_validation->set_message('username_exists', '用户名已被占用');
            return FALSE;
        }
        return TRUE;
    }



/**
     * 用户退出
     * 已经登录则退出，否者转到details
     */
    function logout()
    {
        if ($this->m_account->logout() == TRUE)
        {
            $this->load->view('account/logout');
        }
        else
        {
            $this->load->view('account/index');
        }
        }




	
function index(){
	$this->load->database("default");	
	$this->load->library('pagination');  
	$config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/index/';  	
	$config['total_rows'] = $this->db->count_all('account');     
	$config['per_page'] = '10';	 
	$this->pagination->initialize($config); 	
	$this->load->model("m_account");
	$data['query']=$this->m_account->read($config['per_page'],$this->uri->segment(3));	

	$this->load->view("account/read_header",$data);
	$this->load->view("account/read",$data);
	$this->load->view("account/read_footer",$data);
}
function read(){
	$this->load->database("default");	
	$this->load->library('pagination');  
	$config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/read/';  	
	$config['total_rows'] = $this->db->count_all('account');     
	$config['per_page'] = '10';	 
	$this->pagination->initialize($config); 	
	$this->load->model("m_account");
	$data['query']=$this->m_account->read($config['per_page'],$this->uri->segment(3));	
	
	$this->load->view("account/read_header",$data);
	$this->load->view("account/read",$data);
	$this->load->view("account/read_footer",$data);

}

//管理模块，多项目删除
function manage(){
	$this->load->database("default");	
	$this->load->library('pagination');  
	$config['base_url'] = base_url().'index.php/'.$this->uri->segment(1).'/read/';  	
	$config['total_rows'] = $this->db->count_all('account');     
	$config['per_page'] = '10';	 
	$this->pagination->initialize($config); 	
	$this->load->model("m_account");
	$data['query']=$this->m_account->read($config['per_page'],$this->uri->segment(3));	
	$this->load->view("account/manage",$data);

}



function insert(){
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->database("default");		
		$this->form_validation->set_error_delimiters('', ''); 	
		
		$config = array(
    array("field"   =>"username","label"   => "username","rules"   => "required"),
    array("field"   =>"password","label"   => "password","rules"   => "required"),
    array("field"   =>"email","label"   => "email","rules"   => "required")
);
$this->form_validation->set_rules($config);;
		
	     if ($this->form_validation->run() == FALSE)
  		{   		
               //  $this->load->view("account/insert");
       $this->load->view("account/edit_header");
       $this->load->view("account/insert");
      $this->load->view("account/edit_footer");
  		}
  		else
  		{ 
  		$this->load->helper('url');
  		$this->load->model('m_account','',TRUE);
  		$this->m_account->insert(); 

// 定位到 添加成功后的页面
      $this->load->view("account/submitok");


  	//	redirect('account/insertOk','refresh');	
 		}

}

function insertOk(){
	
	//成功后返回操作
		$segs = $this->uri->segment_array();
 
		echo "<div  style='width:600;height:300;padding:100; margin:100;background:silver'>";
		echo "<h1><li>新建操作成功 </li></h1><br>";  		 
  		echo "<h3><li>".base_url()."index.php/".$segs[1]."/".$segs[2]."<a href=".base_url()."index.php/".$segs[1]."/".$segs[2].">返回上一页</a></li>";  		

  		echo "<li>".base_url()."index.php/".$segs[1]."<a href=".base_url()."index.php/".$segs[1].">返回首页</a></li> ";

  		echo "<li>".base_url()."index.php<a href=".base_url()."index.php>返回网站 首页</a></li></h3>";  

  		//echo '<meta http-equiv="refresh" content="3;URL=".base_url()."index.php/".$segs[1]."/".$segs[2]."><li>三秒后返回前页</li>';

  		echo "<meta http-equiv='refresh' content='3;URL=".base_url()."index.php/".$segs[1]."/'><li>三秒后返回前页</li>";

  		echo "</div>";
  		


	}

function update($a=-1){
		if ($a<0) { 
    exit('Update 后面参数不能为空');
        }

		$this->load->helper('form');
		$this->load->library('form_validation');		
		$this->load->database("default");
		$this->form_validation->set_error_delimiters('', ''); 	
 
 
		$config = array(
    array("field"   =>"username","label"   => "username","rules"   => "required"),
    array("field"   =>"password","label"   => "password","rules"   => "required"),
    array("field"   =>"email","label"   => "email","rules"   => "required")
);
$this->form_validation->set_rules($config);;

	     if ($this->form_validation->run() == FALSE)
  		{   		
                    //  $this->load->view("account/update");
       $this->load->view("account/edit_header");
       $this->load->view("account/update");
      $this->load->view("account/edit_footer");
  		}
  		else
  		{ 
  		$this->load->helper('url');
  		$this->load->model('m_account','',TRUE);
	        $this->m_account->update($a); 
		

  // 定位到 添加成功后的页面
      $this->load->view("account/submitok");
 // redirect('account/updateOk','refresh');  



  		}

}
 


function updateOk(){
  
   
      

  }




function delete($a=-1){
	if ($a<0) { 
		exit('Delete 后面参数不能为空');
				}
				
	$this->load->database("default");
	$this->db->where("id",$a);
	$this->db->delete("account");
	$this->load->helper('url'); 

// 定位到 添加成功后的页面
      $this->load->view("account/submitok");
  		//redirect('account/deleteOk','refresh');	


	}



function delall(){  

	//删除多个id的数据
	//得到从ajax传过来的 数据

	if(isset($_REQUEST['ids'])){
	 	//将字符改为数组 where in 只能接受数组
		$ids=explode(',',$_REQUEST['ids']);
	}else{ 
 		$counts = "0";
 	 	$des = "post没有"; 
  		exit();

	} 

	$this->load->database("default"); 
	$this->db->where_in("id", $ids);	 
	//$this->load->helper('url');  	

	if ($this->db->delete("account")) {
 		$counts = "1";
 		$des = "成功";
	} 
	else {
 		$counts = "0";
  		$des = "失败";
	}  
 
 $json_data="{";
 $json_data.="\"counts\":".json_encode($counts).",";
 $json_data.="\"des\":".json_encode($des).",";  
 $json_data.="\"ids\":".json_encode($ids)."";
 $json_data.="}";
 echo $json_data; 
//break; 
  }





	function deleteOk(){

	//成功后返回操作
		$segs = $this->uri->segment_array();
 
		echo "<div  style='width:600;height:300;padding:100; margin:100;background:silver'>";
		echo "<h1><li>删除操作成功  </li></h1><br>";  		 
  		echo "<h3><li>".base_url()."index.php/".$segs[1]."/".$segs[2]."<a href=".base_url()."index.php/".$segs[1]."/".$segs[2].">返回上一页</a></li>";  		

  		echo "<li>".base_url()."index.php/".$segs[1]."<a href=".base_url()."index.php/".$segs[1].">返回首页</a></li> ";

  		echo "<li>".base_url()."index.php<a href=".base_url()."index.php>返回网站 首页</a></li></h3>";  

  		//echo '<meta http-equiv="refresh" content="3;URL=".base_url()."index.php/".$segs[1]."/".$segs[2]."><li>三秒后返回前页</li>';

  		echo "<meta http-equiv='refresh' content='3;URL=".base_url()."index.php/".$segs[1]."/'><li>三秒后返回前页</li>";

  		echo "</div>";
  		

	} 
function itemlist($a){
	$this->load->database("default");
	$this->load->model("m_account");
	$data['query']=$this->m_account->get1($a);
	
	$this->load->view("account/list_header",$data);
	$this->load->view("account/itemlist",$data);
	$this->load->view("account/list_footer",$data);

}



}
